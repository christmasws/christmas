<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="common/css/bar-ui.css">
    <link rel="stylesheet" type="text/css" href="common/css/style.css">
    <link rel="stylesheet" type="text/css" href="common/css/snowman.css">
    <link rel="stylesheet" type="text/css" href="common/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="common/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />

    <title>Merry Christmas 2015 - WS Group</title>
</head>
<body>
    <div id="container">
        <div id="header"></div>
        <div id="content">
            <div id="countdown_box"><div class="clear_fix row"><img src="images/tuanloc.gif">&nbsp; <span id="countdown"></span></div></div>
            <div id="left_content">
                <snowman>
                    <div class="body">
                        <div class="head"></div>
                        <div class="hat"></div>
                        <div class="scarf"></div>
                    <div class="buttons"></div>
                        <div class="hands">
                            <div class="right-hand"></div>
                            <div class="left-hand"></div>
                        </div>
                        <div class="shadow"></div>
                    </div>
                </snowman>
            </div>
            <div id="right_content"><div id="chrismas_tree"></div></div>
            <div id="large_chrismas_tree">
                <div id="star_large"><img src="images/ct_large_star.png">
                    </div>
                    <div class="wrapper_list">
                        <ul id="list_celebrating">
                            <li id="candycane01" class="gift candy"></li>
                            <li id="candycane02" class="gift candy candyRB"></li>
                            <li id="candycane03" class="gift candy candyRB"></li>
                            <li id="candycane04" class="gift candy"></li>
                            <li id="candycane05" class="gift candy"></li>
                            <li id="candycane06" class="gift candy candyRB"></li>
                            <li id="candycane07" class="gift candy candyRB"></li>

                            <li id="stocking01" class="gift stocking_type01"></li>
                            <li id="stocking02" class="gift stocking_type02"></li>
                            <li id="stocking03" class="gift stocking_type03"></li>
                            <li id="stocking04" class="gift stocking_type04"></li>
                            <li id="stocking05" class="gift stocking_type03"></li>
                            <li id="stocking06" class="gift stocking_type01"></li>
                            <li id="stocking07" class="gift stocking_type02"></li>

                            <li id="gbm1" class="gift rotate-25"></li>
                            <li id="gbm2" class="gift rotate30"></li>
                            <li id="gbm3" class="gift rotate-30"></li>

                            <li id="bell1" class="gift bell rotate-30"></li>
                            <li id="bell2" class="gift bell rotate30"></li>
                            <li id="bell3" class="gift bell rotate-30"></li>
                            <li id="bell4" class="gift bell rotate30"></li>
                            <li id="bell5" class="gift bell rotate-30"></li>
                            <li id="bell6" class="gift bell rotate30"></li>

                        </ul>
                    </div>

            </div>
            <div id="gift_footer"><span class="xmas">Merry &nbsp; X-mas</span></div>
        </div>
    </div>
    <div class="sm2-bar-ui full-width fixed">
        <div class="bd sm2-main-controls">
            <div class="sm2-inline-texture"></div>
            <div class="sm2-inline-gradient"></div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#play" class="sm2-inline-button play-pause">Play / pause</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-inline-status">
                <div class="sm2-playlist">
                    <div class="sm2-playlist-target">
                        <noscript><p>JavaScript is required.</p></noscript>
                    </div>
                </div>
                <div class="sm2-progress">
                    <div class="sm2-row">
                <div class="sm2-inline-time">0:00</div>
                <div class="sm2-progress-bd">
                    <div class="sm2-progress-track">
                        <div class="sm2-progress-bar"></div>
                        <div class="sm2-progress-ball"><div class="icon-overlay"></div></div>
                    </div>
                </div>
                <div class="sm2-inline-duration">0:00</div>
                    </div>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element sm2-volume">
                <div class="sm2-button-bd">
                    <span class="sm2-inline-button sm2-volume-control volume-shade"></span>
                    <a href="#volume" class="sm2-inline-button sm2-volume-control">volume</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#prev" title="Previous" class="sm2-inline-button previous">&lt; previous</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#next" title="Next" class="sm2-inline-button next">&gt; next</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element">
                <div class="sm2-button-bd">
                    <a href="#repeat" title="Repeat playlist" class="sm2-inline-button repeat">&infin; repeat</a>
                </div>
            </div>
            <div class="sm2-inline-element sm2-button-element sm2-menu">
                <div class="sm2-button-bd">
                    <a href="#menu" class="sm2-inline-button menu">menu</a>
                </div>
            </div>
        </div>
        <div class="bd sm2-playlist-drawer sm2-element">
            <div class="sm2-inline-texture">
                <div class="sm2-box-shadow"></div>
            </div>
            <!-- playlist content is mirrored here -->
            <div class="sm2-playlist-wrapper">
                <ul class="sm2-playlist-bd">
                </ul>
            </div>
        </div>
    </div>
</body>
<!-- <script type="text/javascript" src="common/js/snow.js"></script> -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="common/js/soundmanager2.min.js"></script>
<script type="text/javascript" src="common/js/bar-ui.js"></script>
<script type="text/javascript" src="common/js/main.js"></script>
<script type="text/javascript" src="common/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="common/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.gift').bind('click', function(event) {
            $.ajax(
                {
                    url:"controller/random.php",
                    type: 'POST',
                    dataType: 'html',
                    beforeSend: function() {
                        $.fancybox({
                          content : '<p class="loading"><p>',
                          autoDimensions : false,
                          closeClick : false, // prevents closing when clicking INSIDE fancybox
                            openEffect  : 'fade',
                            closeEffect : 'none',
                            helpers   : {
                              overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
                            },
                          closeBtn:false,
                          enableEscapeButton:false
                        });
                    },
                    success: function (data){
                        var __content = "";
                        if (data=="0") {
                            __content = "<div id='content_popup'><p class='popup_result'>Hết quà rồi nhen mắm !!!<p></div>";
                        }else{
                            __content = "<div id='content_popup'><div id='main_content'><p class='popup_result'>Chúc mừng gái nhen !!!</p><p class='popup_result'>Gái đã trúng thưởng quà: <br><br> <b>"+data+"</b></p></div></div>";
                        }
                        $.fancybox({
                          content : __content,
                          autoDimensions : false,
                          closeClick : false, // prevents closing when clicking INSIDE fancybox
                            openEffect  : 'fade',
                            closeEffect : 'true',
                            helpers   : {
                              overlay : {closeClick: true} // prevents closing when clicking OUTSIDE fancybox
                            },
                          closeBtn:true,
                          enableEscapeButton:true
                        });
                    }
                }
            );
            return false;
        });
    });
</script>
</html>