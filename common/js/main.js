var deadline = 'December 24 2015 23:59:59 GMT+07:00';
var countdownTimer = setInterval('getTimeRemaining()', 1000);
function getTimeRemaining(){
        var t = Date.parse(deadline) - Date.parse(new Date());
        var seconds = Math.floor( (t/1000) % 60 );
        var minutes = Math.floor( (t/1000/60) % 60 );
        var hours = Math.floor( (t/(1000*60*60)) % 24 );
        var days = Math.floor( t/(1000*60*60*24) );
        if (t<=0) {
            $('#countdown').html("Merry Christmas !");
        }else{
            $('#countdown').html(days+" days : "+hours+" hours : "+minutes+" minutes : "+seconds+" seconds");
        }
    }
$(document).ready(function(){
    $(window).scroll(function () {
        if ($(window).scrollTop() < 1000) {
            $('#header').addClass("fixed_header");
        } else {
            $('#header').removeClass("fixed_header");
        }
    });
});